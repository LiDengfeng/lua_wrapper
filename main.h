#pragma once
#include <iostream>
#include <stdlib.h>
#include "lua_script.hpp"

#ifdef _WIN32
#include <direct.h>
#include <Windows.h>
#else
#include <unistd.h>
#include <dirent.h>
#endif

using namespace std;

void af_splitpath(const char* s, char* path, char* name, char* ext)
{
	if (path) memset(path, 0, strlen(path));
	if (name) memset(name, 0, strlen(name));
	if (ext) memset(ext, 0, strlen(ext));

	size_t n = strlen(s);
	char* p = (char*)s;
	p += n;
	bool skiped = false;
	while (p != s && *p != '.') {
		if (*p == '/' || *p == '\\') {
			skiped = true;
			break;
		}
		p--;
	}
	char pwholename[256] = { 0 };
	if (*p != '.' || skiped) {
		strcpy(pwholename, s);
	}
	else {
		if (ext) strcpy(ext, p);
		memcpy(pwholename, s, p - s);
	}

	p = pwholename;
	p += n;
	while (p != pwholename && *p != '/' && *p != '\\') p--;
	if (*p == '/' || *p == '\\') {
		p++;
		if (name) strcpy(name, p);
		if (path) memcpy(path, pwholename, p - pwholename - 1);
	}
	else {
		if (name) strcpy(name, pwholename);
		if (path) memcpy(path, ".", 1);
	}
}

int panic(lua_State* l)
{
	std::string reason = "";
	reason += "unprotected error in call to Lua API (";
	const char* s = lua_tostring(l, -1);
	reason += s;
	reason += ")\n";
#ifdef _WIN32
	OutputDebugStringA(reason.c_str());
#endif
	printf("Exception: %s\n", s);
	throw reason;
	return 0;
}
int print(lua_State* l)
{
	std::string s;
	int n = lua_gettop(l);
	lua_getglobal(l, "tostring");

	for (int i = 1; i <= n; ++i)
	{
		lua_pushvalue(l, -1); // function to be called
		lua_pushvalue(l, i);  // value to print
		lua_call(l, 1, 1);

		const char* ret = lua_tostring(l, -1);
		if (!ret)
		{
			//log error
		}
		else
		{
			s.append(ret);
		}
		if (i < n)
		{
			s.append("\t");
		}
		else
		{
			s.append("\n");
		}
		lua_pop(l, 1); //pop result
	}

	printf("[LUA]%s", s.c_str());

	return 0;
}

void myprint()
{
	printf("myprint--------------------aa\n");
}

int myprint2(int a)
{
	printf("myprint2 aaaa a=%d\n", a);
	return 0;
}