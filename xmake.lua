-- project
set_project("lua_wrapper")

-- version
set_version("2.1.5")

-- set xmake min version
set_xmakever("2.1.2")

-- set warning all as error
--set_warnings("all", "error")

-- set language: c99, c++11
set_languages("c99", "cxx11")

-- disable some compiler errors
add_cxflags("-Wno-error=deprecated-declarations", "-fno-strict-aliasing")

-- add defines
add_defines("_GNU_SOURCE=1", "_FILE_OFFSET_BITS=64", "_LARGEFILE_SOURCE")

-- fomit the frame pointer
add_cxflags("-fomit-frame-pointer", "-Wunused-variable","-Wreserved-user-defined-literal")

-- for the windows platform (msvc)
if is_plat("windows") then 

    -- add some defines only for windows
    add_defines("NOCRYPT", "NOGDI")

    -- link libcmt.lib
    add_cxflags("-MD") 

    -- no msvcrt.lib
   -- add_ldflags("-nodefaultlib:\"msvcrt.lib\"")
end

-- for macosx
if is_plat("macosx") then
    add_ldflags("-all_load", "-pagezero_size 10000", "-image_base 100000000")
end

-- for mode coverage
if is_mode("coverage") then
    add_ldflags("-coverage", "-fprofile-arcs", "-ftest-coverage")
end

-- add projects
add_subdirs("../../Unity/build/lua-5.1.5") 

target("wrapper")
	add_deps("lua51")
	set_kind("binary")
	set_objectdir("$(buildir)/$(plat)/$(arch)/.objs")
    set_targetdir("$(projectdir)/$(plat)/$(arch)")
	add_includedirs("$(projectdir)", "../../Unity/build/lua-5.1.5/src")
	-- add links and directory
    add_links(
        "lua51"
        -- ,"bugly"
        -- ,"user32" 
        -- ,"kernel32" --代码用了MessageBox
        -- ,"gdi32"
        -- ,"winspool"
        -- ,"comdlg32"
        -- ,"advapi32"
        -- ,"shell32"
        -- ,"ole32"
        -- ,"oleaut32"
        -- ,"uuid"
        -- ,"odbc32"
        -- ,"odbccp32"
        )
    add_linkdirs("$(projectdir)/$(plat)/$(arch)")

    add_files("main.cpp")
    if is_mode("debug") then
        set_symbols("debug")
        add_cxflags("-Od", "-ZI")
    else
        set_symbols("debug") --设置导出pdb
        add_cxflags("-O2", "-Zi")
    end

target("interface")
    set_kind("shared")
    set_objectdir("$(buildir)/$(plat)/$(arch)/.objs")
    set_targetdir("$(projectdir)/$(plat)/$(arch)")
    add_includedirs("$(projectdir)", "../../Unity/build/lua-5.1.5/src")
    add_linkdirs("$(projectdir)/$(plat)/$(arch)")
    add_deps("lua51")
    add_links("lua51")
    add_files("interface.cpp")

target("testdll")
    set_kind("binary")
    set_objectdir("$(buildir)/$(plat)/$(arch)/.objs")
    set_targetdir("$(projectdir)/$(plat)/$(arch)")
    add_includedirs("$(projectdir)", "../../Unity/build/lua-5.1.5/src")
    add_linkdirs("$(projectdir)/$(plat)/$(arch)")
    add_deps("interface")
    add_links("interface")
    add_files("testdll.cpp")


