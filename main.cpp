﻿// LuaWrapper.cpp : ∂®“Âøÿ÷∆Ã®”¶”√≥Ã–Úµƒ»Îø⁄µ„°£
//
#include "main.h"
#include "lua_wrapper.hpp"

class base_t
{
public:
	void print(){
		printf("base_t::print...\n");
	}

	static void print2(){
		printf("static base_t::print2...\n");
	}

	const char* tostring(){
		printf("call by __tostring\n");
		return "base_t::tostring";
	}
};

class foo_t
{
	std::string _name;
public:
	const int flag = 0;
	int num;
	static int counter;
	foo_t(){
		_name = "class foot";
		num = 0;
		counter++;
	}
	~foo_t(){
		printf("~foo_t\n");
	}
	void setname(const char* name){
		_name = name;
	}
	void fprint(){
		printf("foo_t::print..... name = %s, num = %d, counter = %d\n",_name.c_str(), num, counter);
	}
	static void fprint2(){
		printf("static foo_t::print2.....,counter = %d\n", counter);
	}
};
int foo_t::counter = 0;
int writeonly = 8;

int createfoo(lua_State* l)
{
	foo_t * pf = new foo_t();
	lua::push(l, pf);
	return 1;
}

int printwriteonly(lua_State*)
{
	printf("writeonly = %d\n", writeonly);
	return 0;
}

int main()
{
	char this_path[256] = { 0 };
	af_splitpath(__FILE__, this_path, NULL, NULL);
	printf("this_path:%s\n", this_path);

	lua_State* l = luaL_newstate();
	luaL_openlibs(l);
	lua_atpanic(l, panic);

	lua_pushcfunction(l, print);
	lua_setfield(l, LUA_GLOBALSINDEX, "print");

	/////reg c++ obj
	lua::lua_register_t<base_t>(l, "base_t")
		.def(lua::constructor<>())
		.def(lua::destructor())
		.def(lua::def("print", &base_t::print))
		.def(lua::def("print2", &base_t::print2))
		.def(lua::def("__tostring", &base_t::tostring));

	lua::lua_register_t<foo_t>(l, "foo_t")
		.extend<base_t>()
		.def(lua::constructor<>())
		.def(lua::destructor())
		.def(lua::def("fprint", &foo_t::fprint))
		.def(lua::def("fprint2", &foo_t::fprint2))
		.def(lua::def("setname", &foo_t::setname))
		.def(lua::def("num", &foo_t::num))
		.readonly(lua::def("flag", &foo_t::flag))
		.writeonly(lua::def("writeonly", &writeonly))
		.def(lua::def("counter", &foo_t::counter));

	lua::lua_register_t<void>(l)
		.def("foo", createfoo)
		.def("printwriteonly", printwriteonly);


	char s_luafile[256];
	s_luafile[0] = 0x0;
	sprintf(s_luafile, ("%s/class.lua"), this_path);

	lua::lua_table_ref_t player;
	if(lua::runFile(l, s_luafile))
	{
		lua::pop(l, &player);
		player.call("speak", player, "hello lua.");
		player.call("speak", player, "hello C++.");
		player.unref();
	}
	else
	{
		const char* err = lua_tostring(l, -1);
		printf("can not run class.lua, err = %s!\n", err);
	}

	lua_close(l);

	printf("Success to exit.\n");

#ifdef _WIN32
	system("pause");
#endif
    return 0;
}

