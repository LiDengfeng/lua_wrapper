CC = g++
CFLAGS = -std=c++0x -g -Wno-deprecated
LUA_INCLUDE = -I../../Unity/build/lua-5.1.5/src
LIBRARY = #-lstdc++ -lpthread
LUA_LIB = -L./macosx/x86_64
LUA_CFLAGS = -llua51
CFLAGS2 =

all : test

test : test.o
	$(CC) $(CFLAGS) $(LUA_CFLAGS) -o $@ $^ $(LUA_INCLUDE) $(LUA_LIB)
test.o : main.cpp
	$(CC) -c $(CFLAGS) -o $@ $^ $(LUA_INCLUDE)
clean :
	rm -rf *.o test