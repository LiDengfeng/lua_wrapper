
local function make_instance(instancetype)
	local meta = {}
	meta.d = 10
	meta.__index = function(t, k) 
		print("tt", t, k)
		local meta = getmetatable(t)
		return rawget(meta, k)
	end
	meta.hello = function(t) print("hello") end
	meta.__tostring = function()return "instancetype 11111" end
	setmetatable(instancetype, meta)
end

classtype = {
	-- instancemeta = {
	-- 	__index = function(t, k) 
	-- 		--print("inst__index", t, k)
	-- 		local self = getmetatable(t)
	-- 		if rawget(self, k) then
	-- 			return rawget(self, k)
	-- 		end
	-- 		local meta = getmetatable(classtype)
	-- 		return rawget(meta, k)
	-- 	end,
	-- 	__tostring = function() return "instance 11111" end
	-- }
} 
do
	local meta = {}
	classtype.meta = meta
	meta.d = 10
	meta.__index = function(t, k) 
		local meta = getmetatable(t)
		return rawget(meta, k)
	end
	meta.hello = function(t) print("hello") end
	meta.__tostring = function() return "instancetype 11111" end
end
do
	local classmeta = {}
	classtype.classmeta = classmeta
	classmeta.__call = function(t, a,b)
		local r = {a=a,b=b}
		return setmetatable(r, classtype.meta)
	end
--	classmeta.__tostring = function() return "classtype 11111" end
	classmeta.__index = function(t, k)
		local self = getmetatable(t)
		if rawget(self, k) then
			return rawget(self, k)
		end
		local meta = classtype.meta --getmetatable(classtype)
		return rawget(meta, k)
	end
	setmetatable(classtype, classmeta)
end

for k, v in pairs(getmetatable(classtype)) do
	print(k,v)
end
local v = classtype(3,4)
print(classtype, v)
print(v.a)
print(v.d, v.__index)
v:hello()
print(classtype.d)
classtype.hello(v)