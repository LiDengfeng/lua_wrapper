
quit()
{
	exit ${@+1};
}

xmake f -c -p macosx --xcode_dir=/Volumes/MACOS/Xcode.app || quit 1
xmake -v --backtrace || { echo "error build"; quit 1; }
