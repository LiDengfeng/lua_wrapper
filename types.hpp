/*
 *  File:    types.hpp
 *
 *  Author:  lidengfeng
 *  Date:    2017/06/17
 *  Purpose: 定义常用数据类型
 */
#ifndef __TYPES_HPP__
#define __TYPES_HPP__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string>
#include <vector>


#if defined( _WIN32 )
	#define __std_func		__stdcall
#else
	#define __std_func
#endif//_WIN32

#ifdef   __cplusplus
    #define  __cfunc_begin   extern "C" {
    #define  __cfunc_end     }
#else
	#define  __cfunc_begin
	#define  __cfunc_end
#endif//__cplusplus

#define  __namespace_begin(name)   namespace name {
#define  __namespace_end           }
#define  __namespace(name)         using namespace name;

#if defined(BUILD_AS_DLL)
  #if defined(DLL_API)
    #define __export	 __declspec(dllexport)
  #else
    #define __export	 __declspec(dllimport)
  #endif
#else
  #define __export	     extern
#endif//BUILD_AS_DLL

#ifdef __cplusplus
#if defined(_MSC_VER)
#if _MSC_VER < 1600
	#define 	SUPPORT_CXX11  0
#else
	#define 	SUPPORT_CXX11  1
#endif//_MSC_VER
#else
#if __cplusplus < 201103L
	#define 	SUPPORT_CXX11  0
#else
	#define 	SUPPORT_CXX11  1
#endif//__cplusplus
#endif//_MSC_VER
#endif//__cplusplus

/********************************************************************************/
#ifdef _UNICODE
typedef wchar_t						char_type;
typedef wchar_t*					char_ptr;
typedef std::wstring 				string_type;
typedef std::vector<std::wstring> 	string_list;
typedef std::vector<int>			int_vec;
#define string_len					wcslen
#define string_cmp					wcscmp						
#define string_ncpy					wcsncpy
#define string_cpy 					lstrcpy						
#else
typedef char						char_type;
typedef char*						char_ptr;
typedef std::string 				string_type;
typedef std::vector<std::string> 	string_list;
typedef std::vector<int>			int_vec;
#define string_len 					strlen
#define string_cmp 					strcmp						
#define string_ncpy 				strncpy						
#define string_cpy 					strcpy						
#endif//_UNICODE
/*********************************************************************************/


/********************************************************************************
 
 Base integer types for all target OS's and CPU's
 
 UInt8            8-bit unsigned integer 
 SInt8            8-bit signed integer
 UInt16          16-bit unsigned integer 
 SInt16          16-bit signed integer           
 UInt32          32-bit unsigned integer 
 SInt32          32-bit signed integer   
 UInt64          64-bit unsigned integer 
 SInt64          64-bit signed integer   
 
 *********************************************************************************/
typedef unsigned char                   uchar;
typedef unsigned short                  ushort;
typedef unsigned int                    uint;
typedef unsigned long                   ulong;

typedef signed char                     int8;
typedef uchar                           uint8;
typedef signed short                    int16;
typedef ushort                          uint16;


#ifdef __LP64__
typedef signed int                      int32;
typedef uint                            uint32;
#else
typedef signed long                     int32;
typedef unsigned long                   uint32;
#endif

#if defined(_MSC_VER) && !defined(__MWERKS__) && defined(_M_IX86)
typedef __int64							int64;
typedef unsigned __int64                uint64;
#else
typedef __int64_t						int64;
typedef __uint64_t						uint64;
#endif




#endif//__TYPES_HPP__